View site: http://html-themes.gitlab.io/genius/

----

# GitLab Pages - Plain HTML Example

## Template: [Genius](http://www.templatemo.com/tm-402-genius)

## Distributor: [Templatemo](http://www.templatemo.com/)

## Configuration:

- Default
- GitLab CI: [`.gitlab-ci.yml`](https://gitlab.com/html-themes/genius/blob/master/.gitlab-ci.yml)

## More Info

 - View [Wiki](https://gitlab.com/html-themes/genius/wikis/home)
